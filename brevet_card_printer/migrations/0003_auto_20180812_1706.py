# Generated by Django 2.1 on 2018-08-12 17:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('brevet_card_printer', '0002_auto_20180812_1702'),
    ]

    operations = [
        migrations.AlterField(
            model_name='brevetdata',
            name='nominal_distance',
            field=models.IntegerField(choices=[(200, '200'), (300, '300'), (400, '400'), (600, '600'), (1000, '1000'), (1200, '1200')], max_length=4),
        ),
    ]
