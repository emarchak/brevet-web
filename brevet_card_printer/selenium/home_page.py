class HomePage:
    def __init__(self, driver):
        self.driver = driver

    def header(self):
        return self.driver.find_element_by_tag_name('h1')

    def ad_hoc_link(self):
        return self.driver.find_element_by_id('ad-hoc-link')

    def brevet_list(self):
        return self.driver.find_element_by_id('id_brevet_list')

    def get_brevet_link(self, brevet_name):
        return self.brevet_list().find_element_by_link_text(brevet_name)
