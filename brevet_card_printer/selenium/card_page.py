class CardPage:
    def __init__(self, driver):
        self.driver = driver

    def header(self):
        return self.driver.find_element_by_tag_name('h1')

    def organizer_name_input(self):
        return self.driver.find_element_by_id('organizer-name-input')

    def organizer_email_input(self):
        return self.driver.find_element_by_id('organizer-email-input')

    def organizer_phone_input(self):
        return self.driver.find_element_by_id('organizer-phone-input')

    def rider_name_input(self):
        return self.driver.find_element_by_id('rider-name-input')

    def add_rider_button(self):
        return self.driver.find_element_by_id('add-rider')

    def rider_list(self):
        return self.driver.find_element_by_id('rider-list')

    def get_all_riders(self):
        return [
            e.get_property('value')
            for e
            in self.rider_list().find_elements_by_tag_name('input')
        ]

    def start_date_input(self):
        return self.driver.find_element_by_id('start-date-input')

    def start_time_input(self):
        return self.driver.find_element_by_id('start-time-input')

    def print_button(self):
        return self.driver.find_element_by_id('print-button')
