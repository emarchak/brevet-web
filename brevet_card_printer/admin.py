from django.contrib import admin

from .models import (
    BrevetData,
    ControlData,
)


class ControlInline(admin.StackedInline):
    model = ControlData
    extra = 0


class BrevetAdmin(admin.ModelAdmin):
    fields = ['name', 'nominal_distance']
    inlines = [ControlInline]


admin.site.register(BrevetData, BrevetAdmin)
