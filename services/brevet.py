from datetime import datetime

from .control import Control
from .equivalence import Equivalence
from .time_calc import TimeCalc


class Brevet(Equivalence):
    def __init__(self, brevet_dict, start_time=None):
        if start_time is None:
            start_time = datetime(2000, 1, 1)
        self.name = brevet_dict['name']
        self.distance = brevet_dict['nominal_distance']
        self.__controls = []
        for c_data in brevet_dict['controls']:
            self.__controls.append(Control(c_data['name'], c_data['distance']))
        self.start_time = start_time

    def __str__(self):
        return "{} {}".format(self.name, self.get_nominal_distance())

    def __set_control_times(self, control):
        time_calc = TimeCalc(self.start_time)
        if control.distance >= self.get_nominal_distance():
            control.open_time = time_calc.get_open_time(
                self.get_nominal_distance())
            control.close_time = time_calc.get_nominal_close(
                self.get_nominal_distance())
        else:
            control.open_time = time_calc.get_open_time(
                control.distance)
            control.close_time = time_calc.get_close_time(
                control.distance)

    def __calculate_all_control_times(self):
        for control in self.__controls:
            self.__set_control_times(control)

    @property
    def distance(self):
        return self._distance

    @distance.setter
    def distance(self, value):
        self._distance = int(value)

    @property
    def start_time(self):
        return self.__start_time

    @start_time.setter
    def start_time(self, value):
        self.__start_time = value
        self.__calculate_all_control_times()

    def get_controls(self):
        return self.__controls

    def get_nominal_distance(self):
        if self.distance < 200:
            raise DistanceTooShortError("The distance is less than 200km")
        return self.distance

    def __eq__(self, other):
        result = isinstance(other, self.__class__)
        result = result and self.name == other.name
        result = result and self.distance == other.distance
        for index, control in enumerate(self.get_controls()):
            result = result and control == other.get_controls()[index]
        return result


class DistanceTooShortError(Exception):
    pass
