import os

from reportlab.lib import (
    colors,
    pagesizes,
)
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch
from reportlab.pdfgen import canvas
from reportlab.platypus import (
    Frame,
    Image,
    Paragraph,
    Table,
    TableStyle,
)


class CardMaker:
    def __init__(self):
        self._table_style = None
        self.brevet = None
        self.canvas = None

    def print(self, response, brevet, riders, organizer):
        response['Content-Disposition'] = (
            'attachment; filename="{}.pdf"'.format(brevet)
        )
        self.brevet = brevet
        self.canvas = canvas.Canvas(
            response,
            pagesize=pagesizes.letter,
        )
        self.organizer = organizer
        self.__set_brevet_times()
        self._set_page_dimensions()

        for rider in riders:
            self._split_midpoint()
            self._split_vertical_thirds()
            self._boilerplate_frame()
            self._middle_block_frame()
            self._right_block_frame(rider)
            self._controls()
            self.canvas.showPage()

        self.canvas.save()

        return response

    def _set_page_dimensions(self):
        self._width, self.__height = pagesizes.letter
        self._margin = 0.3 * inch
        self._left = self._margin
        self._right = self._width - self._margin
        self._bottom = self._margin
        self._top = self.__height - self._margin
        self._half_height = self.__height / 2
        self._one_third = self._width / 3
        self._two_thirds = 2 * self._one_third
        self._left_frame_width = self._one_third - self._left
        self._middle_frame_width = self._two_thirds - self._one_third
        self._right_frame_width = self._right - self._two_thirds
        self._frame_height = self._half_height - self._margin

    def _split_midpoint(self):
        y = self.__height / 2
        x1 = self._left
        x2 = self._right
        self.canvas.line(x1, y, x2, y)

    def _split_vertical_thirds(self):
        y1 = self._bottom
        y2 = self._top
        x1 = self._one_third
        x2 = self._two_thirds
        self.canvas.line(x1, y1, x1, y2)
        self.canvas.line(x2, y1, x2, y2)

    def __set_brevet_times(self):
        controls = self.brevet.get_controls()
        self.__brevet_start_date = self.brevet.start_time.strftime("%d-%b-%Y")
        self.__brevet_start_time = self.brevet.start_time.strftime("%H:%M")
        self.__brevet_finish_date = controls[-1].close_time.strftime(
            "%d-%b-%Y")
        self.__brevet_finish_time = controls[-1].close_time.strftime("%H:%M")
        hours, remainder = divmod(
            (controls[-1].close_time - controls[0].open_time).total_seconds(), 3600)
        self.__brevet_allowable_hours = round(hours)
        self.__brevet_allowable_minutes = round(remainder / 60)

    def _boilerplate_frame(self):
        frame = Frame(
            self._left,
            self._half_height,
            self._left_frame_width,
            self._frame_height,
            leftPadding=0,
            topPadding=0,
        )
        style = self.get_style(font_size=7)
        story = [
            Paragraph(self.regulations_text(), style),
            Paragraph(self.sag_wagon_text(), style),
            Paragraph(self.control_card_text(), style),
            Paragraph(self.conduct_text(), style),
            Paragraph(self.cycle_text(), style),
            Paragraph(self.assistance_text(), style),
        ]
        frame.addFromList(story, self.canvas)

    def _middle_block_frame(self):
        frame = Frame(
            self._one_third,
            self._half_height,
            self._middle_frame_width,
            self._frame_height,
        )
        style = self.get_style(font_size=8)
        style.spaceAfter = 10
        machine_style = self.get_machine_style()

        story = [
            Paragraph(self.start_time_text(), style),
            Paragraph(self.finish_time_text(), style),
            Paragraph(self.allowable_time_text(), style),
            Paragraph(self.actual_time_text(), style),
            Paragraph(self.hours_text(), style),
            Paragraph(self.minutes_text(), style),
            Paragraph(self.machine_examiner_text(), machine_style),
            Paragraph(self.official_signature_text(), machine_style),
        ]
        frame.addFromList(story, self.canvas)

    def _right_block_frame(self, rider):
        frame = Frame(
            self._two_thirds,
            self._half_height,
            self._right_frame_width,
            self._frame_height,
        )
        style = self.get_style(10)
        style.spaceAfter = 10
        image_path = os.path.join(os.path.dirname(__file__), "logo.jpg")
        img = Image(image_path)
        img.drawWidth = .9 * self._right_frame_width
        img.drawHeight = img.imageHeight * img.drawWidth / img.imageWidth
        story = [
            img,
            Paragraph(self.event_text(), style),
            Paragraph(self.rider_text(rider), style),
            Paragraph(self.date_text(), style),
            Paragraph(self.event_rules_text(), style),
            Paragraph(self.emergency_text(), style),
        ]
        story.extend(self.organizer_para())
        frame.addFromList(story, self.canvas)

    def _controls(self):
        ctl_frame_left = Frame(
            self._left,
            self._bottom,
            self._left_frame_width,
            self._frame_height,
            leftPadding=0,
            rightPadding=0,
            topPadding=0,
        )
        ctl_frame_middle = Frame(
            self._one_third,
            self._bottom,
            self._middle_frame_width,
            self._frame_height,
            leftPadding=0,
            rightPadding=0,
            topPadding=0,
        )
        ctl_frame_right = Frame(
            self._two_thirds,
            self._bottom,
            self._right_frame_width,
            self._frame_height,
            leftPadding=0,
            rightPadding=0,
            topPadding=0,
        )
        style = self.get_style(9)
        data = [['Control', 'Signature']]
        data.extend(self._control_cells(style))
        table = self._control_table(data, self._left_frame_width)
        split_tables = ctl_frame_left.split(table, self.canvas)
        ctl_frame_left.add(split_tables[0], self.canvas)
        if len(split_tables) > 1:
            table = self._control_table(
                split_tables[1]._cellvalues,
                self._middle_frame_width
            )
            split_tables = ctl_frame_middle.split(table, self.canvas)
            ctl_frame_middle.add(split_tables[0], self.canvas)
        if len(split_tables) > 1:
            table = self._control_table(
                split_tables[1]._cellvalues,
                self._right_frame_width
            )
            split_tables = ctl_frame_right.split(table, self.canvas)
            ctl_frame_right.add(split_tables[0], self.canvas)

    @property
    def table_style(self):
        if not self._table_style:
            self._table_style = TableStyle(
                [
                    ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                    ('BOX', (0, 0), (-1, -1), 1, colors.black),
                ]
            )
        return self._table_style

    def _control_table(self, data, frame_width):
        table = Table(
            data,
            repeatRows=1,
            colWidths=(
                .6 * frame_width,
                .4 * frame_width
            ),
        )
        table.setStyle(self.table_style)
        return table

    def _control_cells(self, style):
        return [
            [self._control_cell(c, style), '']
            for c in self.brevet.get_controls()
        ]

    def _control_cell(self, control, style):
        return [
            Paragraph(
                '{} km {}'.format(control.distance, control.name), style
            ),
            Paragraph(
                'Open: {}'.format(control.open_time_text), style
            ),
            Paragraph(
                'Close: {}'.format(control.close_time_text), style
            ),
        ]

    def get_style(self, font_size):
        stylesheet = getSampleStyleSheet()
        style = stylesheet['Normal']
        style.fontSize = font_size
        return style

    def get_machine_style(self):
        machine_style = self.get_style(8)
        machine_style.alignment = 1
        machine_style.spaceBefore = 40
        machine_style.borderPadding = (30, 2, 2, 2)
        machine_style.borderWidth = 1
        machine_style.borderColor = colors.black
        return machine_style

    def regulations_text(self):
        return (
            u'<b>Regulations:</b> Each participant is to be considered on a '
            u'private excursion and remains responsible for any accidents in '
            u'which they may be involved. Each participant is responsible for '
            u'following the route. Although Randonneurs Ontario will endeavour '
            u'to ensure that all route instructions are correct, no '
            u'responsibility can be accepted for participants becoming lost. '
            u'Should a participant become lost or stranded by mechanical '
            u'problems or fatigue, it will be their responsibility to get '
            u'home.')

    def sag_wagon_text(self):
        return (
            '<b>There will be no SAG wagon.</b>'
        )

    def control_card_text(self):
        return (
            '<b>Control Card:</b> The participant to whom this card is issued '
            'must present it at each control for the official stamp, '
            'signature, and control time. Loss of this card, or the absence '
            'of any of the control stamps, or any irregularity in stamping '
            'or signing of the card will result in disqualification.'
        )

    def conduct_text(self):
        return (
            '<b>Conduct:</b> Participants must at all times obey the rules '
            'of the road and conduct themselves in a manner which will not '
            'discredit the Randonnuers Ontario organization. Failure to do '
            'so will result in disqualification.'
        )

    def cycle_text(self):
        return (
            '<b>Cycle:</b> Any cycle is permitted (bicycle, tandem, tricycle, '
            'etc.) providing it is powered by muscle power alone. Powerful '
            'front and rear lights must be attached to the cycle day and '
            'night. The cycle must be in good mechanical condition to '
            'participate in the event.'
        )

    def assistance_text(self):
        return (
            u'<b>Assistance:</b> Each participant must provide for his/her '
            u'needs during the event. Following vehicles are not permitted. '
            u'Mechanical and personal assistance may only be received '
            u'at control points.')

    def start_time_text(self):
        return (
            u'Schedule start time is <b>{}</b> on <b>{}</b>'.format(
                self.__brevet_start_time,
                self.__brevet_start_date,
            )
        )

    def finish_time_text(self):
        return (
            u'Schedule finish time is <b>{}</b> on <b>{}</b>'.format(
                self.__brevet_finish_time,
                self.__brevet_finish_date,
            )
        )

    def allowable_time_text(self):
        return (
            u'Total allowable time is <b>{}</b> hours '
            u'and <b>{}</b> minutes'.format(
                self.__brevet_allowable_hours,
                self.__brevet_allowable_minutes,
            )
        )

    def actual_time_text(self):
        return(u'Time rider completed distance:')

    def hours_text(self):
        return u'<para spaceAfter="10">Hours:</para>'

    def minutes_text(self):
        return u'<para spaceAfter="10">Minutes:</para>'

    def machine_examiner_text(self):
        return u'Machine Examiner\'s Stamp & Signature'

    def official_signature_text(self):
        return u'Signature of Official'

    def event_text(self):
        return u'Event: <b>{}</b>'.format(self.brevet)

    def rider_text(self, rider):
        return u'Rider: <b>{}</b>'.format(rider)

    def date_text(self):
        return u'Date: <b>{}</b>'.format(self.__brevet_start_date)

    def event_rules_text(self):
        return (
            u'Event organized under the rules and regulations of '
            u'Les Randonneurs Mondiaux.'
        )

    def emergency_text(self):
        return (
            u'In Emergency, contact <b>911</b>'
        )

    def organizer_para(self):
        style = self.get_style(8)
        paras = []
        if self.organizer['name'] == '':
            return [Paragraph('', style)]
        paras.append(Paragraph(u'Organizer:', style))
        paras.append(
            Paragraph(u'Name: {}'.format(self.organizer['name']), style)
        )
        if self.organizer['email'] != '':
            paras.append(
                Paragraph(u'Email: {}'.format(self.organizer['email']), style)
            )
        if self.organizer['phone'] != '':
            paras.append(
                Paragraph(u'Phone: {}'.format(self.organizer['phone']), style))
        return paras
