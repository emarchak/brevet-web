__author__ = 'Stephen'

import unittest
from datetime import datetime, timedelta

from services.brevet import Brevet
from services.tests.fixtures import Fixtures


class TestBrevet(unittest.TestCase):
    def test_bewdley(self):
        brevet = Fixtures().get_bewdley_brevet()
        self.assertEqual(200, brevet.get_nominal_distance())
        self.assertEqual(datetime(2000, 1, 1), brevet.get_controls()[0].open_time)
        self.assertEqual(datetime(2000, 1, 1, 1), brevet.get_controls()[0].close_time)
        self.assertEqual(datetime(2000, 1, 1, 2, 5), brevet.get_controls()[1].open_time)
        self.assertEqual(datetime(2000, 1, 1, 4, 44), brevet.get_controls()[1].close_time)
        self.assertEqual(datetime(2000, 1, 1, 2, 55), brevet.get_controls()[2].open_time)
        self.assertEqual(datetime(2000, 1, 1, 6, 36), brevet.get_controls()[2].close_time)
        self.assertEqual(datetime(2000, 1, 1, 3, 46), brevet.get_controls()[3].open_time)
        self.assertEqual(datetime(2000, 1, 1, 8, 32), brevet.get_controls()[3].close_time)
        self.assertEqual(datetime(2000, 1, 1, 5, 53), brevet.get_controls()[4].open_time)
        self.assertEqual(datetime(2000, 1, 1, 13, 30), brevet.get_controls()[4].close_time)

    def test_equals(self):
        b1 = Fixtures().get_bewdley_brevet()
        b2 = Fixtures().get_bewdley_brevet()
        self.assertEqual(b1, b2)

    def test_not_equals(self):
        b1 = Fixtures().get_bewdley_brevet()
        b2 = Fixtures().get_bewdley_brevet()
        b2.name = "Other"
        self.assertNotEqual(b1, b2)

    def test_lol(self):
        lol = Fixtures().get_lol_brevet()
        self.assertEqual(1000, lol.get_nominal_distance())
        self.assertEqual(datetime(2000, 1, 1), lol.get_controls()[0].open_time)
        self.assertEqual(datetime(2000, 1, 1, 1), lol.get_controls()[0].close_time)
        self.assertEqual(datetime(2000, 1, 1, 4, 16), lol.get_controls()[1].open_time)
        self.assertEqual(datetime(2000, 1, 1, 9, 40), lol.get_controls()[1].close_time)
        self.assertEqual(datetime(2000, 1, 1, 5, 49), lol.get_controls()[2].open_time)
        self.assertEqual(datetime(2000, 1, 1, 13, 12), lol.get_controls()[2].close_time)
        self.assertEqual(datetime(2000, 1, 1, 8, 53), lol.get_controls()[3].open_time)
        self.assertEqual(datetime(2000, 1, 1, 19, 44), lol.get_controls()[3].close_time)
        self.assertEqual(datetime(2000, 1, 1, 9, 42), lol.get_controls()[4].open_time)
        self.assertEqual(datetime(2000, 1, 1, 21, 28), lol.get_controls()[4].close_time)
        self.assertEqual(datetime(2000, 1, 1, 13, 10), lol.get_controls()[5].open_time)
        self.assertEqual(datetime(2000, 1, 2, 4, 44), lol.get_controls()[5].close_time)
        self.assertEqual(datetime(2000, 1, 1, 17, 14), lol.get_controls()[6].open_time)
        self.assertEqual(datetime(2000, 1, 2, 12, 52), lol.get_controls()[6].close_time)
        self.assertEqual(datetime(2000, 1, 1, 18, 44), lol.get_controls()[7].open_time)
        self.assertEqual(datetime(2000, 1, 2, 15, 52), lol.get_controls()[7].close_time)
        self.assertEqual(datetime(2000, 1, 1, 21, 24), lol.get_controls()[8].open_time)
        self.assertEqual(datetime(2000, 1, 2, 22, 23), lol.get_controls()[8].close_time)
        self.assertEqual(datetime(2000, 1, 2, 0, 46), lol.get_controls()[9].open_time)
        self.assertEqual(datetime(2000, 1, 3, 6, 37), lol.get_controls()[9].close_time)
        self.assertEqual(datetime(2000, 1, 2, 3, 54), lol.get_controls()[10].open_time)
        self.assertEqual(datetime(2000, 1, 3, 14, 19), lol.get_controls()[10].close_time)
        self.assertEqual(datetime(2000, 1, 2, 6, 5), lol.get_controls()[11].open_time)
        self.assertEqual(datetime(2000, 1, 3, 19, 39), lol.get_controls()[11].close_time)
        self.assertEqual(datetime(2000, 1, 2, 9, 5), lol.get_controls()[12].open_time)
        self.assertEqual(datetime(2000, 1, 4, 3), lol.get_controls()[12].close_time)
        self.assertEqual("Lake Ontario Loop 1000", str(lol))

    def test_300(self):
        brevet = Fixtures().get_300_brevet()
        self.assertEqual(datetime(2000, 1, 1, 20), brevet.get_controls()[0].close_time)

    def test_400(self):
        brevet = Fixtures().get_400_brevet()
        self.assertEqual(datetime(2000, 1, 2, 3), brevet.get_controls()[0].close_time)

    def test_600(self):
        brevet = Fixtures().get_600_brevet()
        self.assertEqual(datetime(2000, 1, 2, 16), brevet.get_controls()[0].close_time)

    def test_1200(self):
        brevet = Fixtures().get_1200_brevet()
        self.assertEqual(datetime(2000, 1, 4, 18), brevet.get_controls()[0].close_time)

    def test_get_close_perfect_200(self):
        brevet=Fixtures().get_perfect_200()
        expected=brevet.start_time + timedelta(hours=13.5)
        self.assertEqual(expected, brevet.get_controls()[-1].close_time)

if __name__ == '__main__':
    unittest.main()
